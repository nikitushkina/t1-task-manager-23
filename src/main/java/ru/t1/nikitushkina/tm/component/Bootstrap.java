package ru.t1.nikitushkina.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.ICommandRepository;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.api.repository.ITaskRepository;
import ru.t1.nikitushkina.tm.api.repository.IUserRepository;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.command.project.*;
import ru.t1.nikitushkina.tm.command.system.*;
import ru.t1.nikitushkina.tm.command.task.*;
import ru.t1.nikitushkina.tm.command.user.*;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nikitushkina.tm.exception.system.CommandNotSupportedException;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.repository.CommandRepository;
import ru.t1.nikitushkina.tm.repository.ProjectRepository;
import ru.t1.nikitushkina.tm.repository.TaskRepository;
import ru.t1.nikitushkina.tm.repository.UserRepository;
import ru.t1.nikitushkina.tm.service.*;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserRemoveCommand());
        registry(new UserUnlockCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1pwd", "user1@a.ru");
        @NotNull final User user2 = userService.create("user2", "user2pwd", "user2@a.ru");
        @NotNull final User user3 = userService.create("user3", "user3pwd", Role.ADMIN);

        projectService.add(user1.getId(), new Project("Project 02", "p02", Status.NOT_STARTED));
        projectService.add(user2.getId(), new Project("Project 03", "p03", Status.COMPLETED));
        projectService.add(user3.getId(), new Project("Project 01", "p01", Status.IN_PROGRESS));

        taskService.create(user1.getId(), "Task 03", "t03");
        taskService.create(user2.getId(), "Task 01", "t01");
        taskService.create(user3.getId(), "Task 02", "t02");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        System.exit(0);
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String argument = args[0];
        processArgument(argument);
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (args.length > 0) processArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
