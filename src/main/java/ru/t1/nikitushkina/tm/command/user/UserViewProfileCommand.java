package ru.t1.nikitushkina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-user-profile";

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN:" + user.getLogin());
        System.out.println("FIRST NAME:" + user.getFirstName());
        System.out.println("MIDDLE NAME:" + user.getMiddleName());
        System.out.println("LAST NAME:" + user.getLastName());
        System.out.println("E-MAIL:" + user.getEmail());
        System.out.println("ROLE:" + user.getRole());
    }

}
